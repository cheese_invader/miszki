//
//  main.swift
//  bruteforce
//
//  Created by Alexander Saveliev on 20.12.2019.
//  Copyright © 2019 Alexander Saveliev. All rights reserved.
//

import Foundation

let KOI8_R = String.Encoding(rawValue: CFStringConvertEncodingToNSStringEncoding(CFStringEncoding(CFStringEncodings.KOI8_R.rawValue)))

let currentUrl = URL(string: "file://\(FileManager.default.currentDirectoryPath)")!

let inFileUrl = currentUrl.appendingPathComponent(CommandLine.arguments[1])
let outFileUrl = currentUrl.appendingPathComponent(CommandLine.arguments[2])

let special : [UInt8] = [0xA, 0xD]
let punctuation : [UInt8] = [0x2C, 0x2E, 0x3A, 0x20, 0x22, 0x2D, 0x3F, 0x28, 0x29]
let digits      : [UInt8] = Array(UInt8(0x30) ... UInt8(0x39))
let cyrillic: [UInt8] = Array(UInt8(0xC0) ... UInt8(0xFF))
let latin   : [UInt8] = Array(UInt8(0x41) ... UInt8(0x5A))
let latinSmall : [UInt8] = Array(UInt8(0x61) ... UInt8(0x75))
let alphabet = special + punctuation + cyrillic + latin + latinSmall + digits

let keyAlphabet = cyrillic + digits


guard let data = try? Data(contentsOf: inFileUrl) else {
    fatalError("Can't read input file")
}

let encryptedBytes = [UInt8](data)


func brutKeyLength(_ keyLength: Int) -> [UInt8]? {
    var sets = [Set<UInt8>]()
    
    for offset in 0 ..< keyLength {
        var set = Set<UInt8>()
        
        for i in stride(from: offset, to: encryptedBytes.count, by: keyLength) {
            let encryptedByte = encryptedBytes[i]
            var currentSet = Set<UInt8>()
            
            for letter in alphabet {
                for keyByte in keyAlphabet where set.isEmpty || set.contains(keyByte) {
                    if keyByte &+ letter == encryptedByte {
                        currentSet.insert(keyByte)
                    }
                }
            }
                        
            guard !currentSet.isEmpty else { return nil }
            if set.isEmpty {
                set = currentSet
            } else {
                set = set.intersection(currentSet)
            }
            guard !set.isEmpty else { return nil }
        }
        
        sets.append(set)
    }
    return sets.map { $0.first! }
}

for keyLength in 1 ..< 20 {
    print(keyLength)
    if let res = brutKeyLength(keyLength) {
        print("Key: \(String(data: Data(res), encoding: KOI8_R)!)")
        break
    }
}
