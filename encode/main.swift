//
//  main.swift
//  encode
//
//  Created by Alexander Saveliev on 15.12.2019.
//  Copyright © 2019 Alexander Saveliev. All rights reserved.
//

import Foundation

let KOI8_R = String.Encoding(rawValue: CFStringConvertEncodingToNSStringEncoding(CFStringEncoding(CFStringEncodings.KOI8_R.rawValue)))

let currentUrl = URL(string: "file://\(FileManager.default.currentDirectoryPath)")!

let inFileUrl = currentUrl.appendingPathComponent(CommandLine.arguments[1])
let outFileUrl = currentUrl.appendingPathComponent(CommandLine.arguments[2])
let key = CommandLine.arguments[3]


func encode(_ data: Data, key: Data) -> Data {
    let bytesToEncode = [UInt8](data)
    let bytesKey      = [UInt8](key)
    var encodedBytes  = [UInt8]()
    
    for (i, byte) in bytesToEncode.enumerated() {
        encodedBytes.append(byte &+ bytesKey[(i % bytesKey.count)])
    }
    
    return Data(encodedBytes)
}


guard let data = try? Data(contentsOf: inFileUrl) else {
    fatalError("Can't read input file")
}

let str = String(data: data, encoding: .utf8)!


let dataToEncode = str.data(using: KOI8_R)!

let keyBytes = key.data(using: KOI8_R)!
let encoded = encode(dataToEncode, key: keyBytes)

try! encoded.write(to: outFileUrl)

