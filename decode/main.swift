//
//  main.swift
//  decode
//
//  Created by Alexander Saveliev on 15.12.2019.
//  Copyright © 2019 Alexander Saveliev. All rights reserved.
//

import Foundation

let KOI8_R = String.Encoding(rawValue: CFStringConvertEncodingToNSStringEncoding(CFStringEncoding(CFStringEncodings.KOI8_R.rawValue)))

let currentUrl = URL(string: "file://\(FileManager.default.currentDirectoryPath)")!

let inFileUrl = currentUrl.appendingPathComponent(CommandLine.arguments[1])
let outFileUrl = currentUrl.appendingPathComponent(CommandLine.arguments[2])
let key = CommandLine.arguments[3]

func decode(_ data: Data, key: Data) -> Data {
    let bytesToDecode = [UInt8](data)
    let bytesKey      = [UInt8](key)
    var decodedBytes  = [UInt8]()
    
    for (i, byte) in bytesToDecode.enumerated() {
        decodedBytes.append(byte &- bytesKey[(i % bytesKey.count)])
    }
    
    return Data(decodedBytes)
}


guard let data = try? Data(contentsOf: inFileUrl) else {
    fatalError("Can't read input file")
}

let keyBytes = key.data(using: KOI8_R)!
let decoded = decode(data, key: keyBytes)

let message = String(data: decoded, encoding: KOI8_R)!

try! message.write(to: outFileUrl, atomically: true, encoding: .utf8)
